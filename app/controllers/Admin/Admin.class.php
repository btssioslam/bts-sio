<?php
namespace App\Controllers;

use App\Database\Database;
use App\Models\Router;

class Admin extends Router
{
	public function __construct()
	{
		Database::getInstance();
	}

	public function index()
	{
		$array = array(
			'title'=> 'Admin Page',
			'scripts' => array(),
		);

		$id = 1;

		$req = $this->select(array('pseudo, password'))
			->from('Users', 'u')
			->where(array('idUser', '=', ':id'))
			->prep(array(':id' => $this->escapeString($id)))
			->fetch('single', 'obj');

		$this->render($array);
	}
}
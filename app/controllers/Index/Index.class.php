<?php
namespace App\Controllers;

use App\Database\Database;
use App\Models\Router;

class Index extends Router
{
	public function __construct()
	{
		Database::__construct();
	}

	public function index()
	{
		$array = array(
			'title' => 'Page accueil',
		);

		$this->render($array);
	}
}
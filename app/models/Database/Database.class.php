<?php
namespace App\Database;
/**
 * Class Database
 * The class is based on the ORM Doctrine by the definition of methods
 * The purpose is to separate all functions to execute a query
 * and gain in lisibylity for the code, and also the simplicity to execute queries
 * @author M.Tahitoa
 * @date 16/04/2016
 * @release 0.0.1
 * @since 16/04/2016
 * @contributors :
 * @class : Database
 * @access abstract
 */
const LOGIN = "bts-dev";
const PASSWD = "BTS$10D3V";
const DATABASE = "btsdev";
const DRIVER = "mysql";
const HOST = "localhost";
const ENV = TRUE;

abstract class Database extends \PDO implements QueryBuilder
{
	/**
	 * The var for the PDO instance
	 * @var \PDO
	 * @access protected
	 */

	protected static $instance;

	protected $_req;
	protected $_alias = array();
	protected $_fetch;
	protected $_exec;
	protected $_executed;

	protected $fetch = array(
		'all'    => 'fetchAll',
		'single' => 'fetch',
		'col'    => 'fetchColumn',
	);

	protected $typeFetch = array(
		'assoc' => \PDO::FETCH_ASSOC,
		'both'  => \PDO::FETCH_BOTH,
		'obj'   => \PDO::FETCH_OBJ,
		'class' => \PDO::FETCH_CLASS,
		'col'   => \PDO::FETCH_COLUMN,
	);

	protected $typeParam = array(
		'string'  => \PDO::PARAM_STR,
		'integer' => \PDO::PARAM_INT,
		'double'  => \PDO::PARAM_INT,
		'null'    => \PDO::PARAM_NULL,
		'boolean' => \PDO::PARAM_BOOL,
	);

	/**
	 * Database constructor.
	 */
	public function __construct()
	{
		$dns = DRIVER.':host='.HOST.';dbname='.DATABASE;
		try
		{
			$pdo = new \PDO($dns, LOGIN, PASSWD, array());

			if(ENV)
			{
				$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			}
			else
			{

			}

			$pdo->exec('USE '.DATABASE);
			$pdo->exec('SET NAMES UTF8');

			$this::$instance = $pdo;
		}
		catch(\PDOException $e)
		{
			print "Erreur de connexion <br />";
			print "Information complémentaires : " . $e->getMessage();
		}
	}

	/**
	 * Method to return the instance if exists, or to create it
	 * @access public
	 * @author M. Tahitoa
	 * @contributors :
	 * @version 0.0.1
	 * @package Database
	 * @return \PDO
	 */
	public static function getInstance()
	{
		if(!self::$instance)
		{
			return new self::$instance;
		}

		return self::$instance;
	}

	public function __clone()
	{
		return false;
	}

	public function __destruct()
	{
		return true;
	}

	/**
	 * Method to insert error catch in the DB
	 * @access public
	 * @version 0.0.1
	 * @package Database
	 * @author M. Tahitoa
	 * @contributors : 
	 * @param \Exception $error
	 */
	public function setErrorPDO(\Exception $error)
	{
		$message = $error->getMessage();
		$code = $error->getCode();
		$file = $error->getFile();
		$line = $error->getLine();

		$this->_req = "";
		$this->insert('Error', 'er')
			->fields(array('messageError, codeError, file, lineFile'))
			->values(array(':mess, :code, :file, :line'))
			->prep()
			->setParam(':mess', $message)
			->setParam(':code', $code)
			->setParam(':file', $file)
			->setParam(':line', $line)
			->execPrepared();

		$this->_req = "";
	}
	
	/**
	 * Method to prepare a select request
	 * @access public
	 * @author M. Tahitoa
	 * @package Database
	 * @param array $fields Fields to request
	 * @return $this
	 * 
	 * Sample : 
	 * $req = $this->select(array('username, password'))
	 *              ->from('users', 'u')
	 *              ->execute()
	 *              ->fetch('all', 'obj');
	 */
	public function select($fields = array())
	{
			if(empty($this->_req))
			{
				$req = "SELECT " . $fields[0] .' ';
				$this->_req .= $req;
			}
			else
			{
				$this->_req .= $fields[0] . ' ';
			}
		return $this;
	}
	
	/**
	 * Method to update a table
	 * @param string $table The table to update
	 * @param null|string $alias If need to put an alias
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors : 
	 * @return $this
	 * 
	 * Sample : 
	 * $req = $this->update('user', 'u')
	 *          ->set('username', ':new_pseudo'))
	 *          ->where(array('id', '=', 1))
	 *          ->setParam(':new_pseudo', 'test')
	 *          ->execReq();
	 *
	 *
	 * $req = $this->update('user', 'u')
	 *          ->set('username', ':new_pseudo,') //take a look at the comma
	 *          ->set('password', 'new_password')) //cause of the second param to edit
	 *          ->where(array('id', '=', 1))
	 *          ->setParam(':new_pseudo', 'test')
	 *          ->setParam(':new_password', 'newpassword')
	 *          ->execReq();
	 *
	 */
	public function  update($table, $alias = null)
	{
		if(!empty($alias))
		{
			$this->_req .= "UPDATE ".$table." AS ".$alias ." ";
		}
		else
		{
			$this->_req .= "UPDATE ".$table." ";
		}
		return $this;
	}

	/**
	 * Method to delete an entry in the database
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @param string $table Concerned table
	 * @return $this
	 *
	 * Sample :
	 *
	 * $req = $this->delete('user')
	 *        ->where('iduser', '=', 1)
	 *        ->execute();
	 */

	public function delete($table)
	{
		$this->_req = "DELETE FROM " .$table . " ";
		return $this;
	}

	/**
	 * Method to insert a new entry in the database
	 * @access public
	 * @author M. Tahitoa
	 * @contributors :
	 * @version 0.0.1
	 * @package Database
	 * @param string $table Concerned table
	 * @param null|string $alias To use an alias
	 * @return $this
	 * $
	 * Sample :
	 *
	 * $req = $this->insert('user', 'u')
	 *          ->fields(array('username, password, email'))
	 *          ->values(array(':pseudo, :password, :mail'))
	 *          ->setParam(':pseudo', 'pseudo')
	 *          ->setParam(':password', 'password')
	 *          ->setParam(':mail', 'mail@mail.com')
	 *          ->execReq();
	 */
	public function insert($table, $alias = null)
	{
		$this->_req .= "INSERT INTO ".$table ."(";
		return $this;
	}

	/**
	 * Method to specify fields in a request
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @param array $fields Fields to select
	 * @return $this
	 *
	 * Sample :
	 *
	 * $req = $this->select('user', 'u')
	 *              ->fields(array('username, password'))
	 *              ->execute()
	 *              ->fetch('all', 'obj');
	 */
	public function fields($fields = array())
	{
		$this->_req .= $fields[0].') ';
		return $this;
	}

	/**
	 * Method to use with insert() method
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @param array $values Values to insert
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->insert('table')
	 *              ->fields(array('column, column1, column2'))
	 *              ->values(array(':col, :col1, :col2'))
	 *              ->prepare()
	 *              ->setParam(':col', 'val')
	 *              ->setParam(':col1', 'val1')
	 *              ->setParam(':col2', 'val2')
	 *              ->execReq();
	 */
	public function values($values = array())
	{
		$this->_req .= 'VALUE('.$values[0].') ';

		return $this;
	}

	/**
	 * Method to design the table target
	 * @param string $table Targeted table
	 * @param null|string $alias To define an alias
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 *
	 *$req = $this->select(array('*'))
	 *             ->from('table', 't')
	 *             ->execute()
	 *             -fetch('all', 'obj');
	 */
	public function from($table, $alias = null)
	{
		if(!empty($alias))
		{
			$this->_req .= 'FROM '.$table . ' AS '.$alias .' ';
		} else
		{
			$this->_req .= ' FROM '.$table;
		}

		return $this;
	}

	/**
	 * Method to join a table
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @param string $table Target table for join
	 * @param null|string $alias To use an alias
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('*'))
	 *             ->from('Users', 'u')
	 *             ->join('Access', 'acc')
	 *             ->on('u.idAccess', 'acc.idAccess')
	 *             ->execute()
	 *             ->fetch('all', 'obj');
	 */
	public function join($table, $alias = null)
	{
		$this->_req .= "JOIN " . $table . " AS " .$alias . " ";
		return $this;
	}

	/**
	 * Method used with method Database::join()
	 * @param string $table1
	 * @param string $table2
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @require Database::join()
	 * @return $this
	 *
	 * Sample :
	 * Take a look the join's sample.
	 */
	public function on($table1, $table2)
	{
		$this->_req .= "ON ".$table1 . "=".$table2 . " ";
		return $this;
	}

	/**
	 * Method to make a sum
	 * @param string $field Field to use
	 * @param string $alias Name the sum to use it later
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->sum('price', 'total price')
	 *              ->from('table', 't')
	 *              ->execute()
	 *              ->fetch('single', 'obj');
	 */
	public function sum($field, $alias)
	{
		$this->_req .= "SELECT SUM(".$field.") AS ".$alias.", ";
		return $this;
	}

	/**
	 * Method to count the entries
	 * @param string $field What count | * to design all
	 * @param string $alias Name the count to use it later
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->count('users', 'totalUsers')
	 *              ->from('users', 'u')
	 *              ->execute()
	 *              ->fetch('all', 'obj');
	 */
	public function count($field, $alias)
	{
		$this->_req .= "SELECT COUNT(".$field.") AS ".$alias.", ";
		return $this;
	}

	/**
	 * Method to make an average
	 * @param string $field Field concerned
	 * @param string $alias Name the average to use it later
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->avg('price', 'AveragePrice')
	 *              ->from('products', 'prod')
	 *              ->execute()
	 *              ->fetch('all', 'obj');
	 */
	public function avg($field, $alias)
	{
		$this->_req .= "SELECT AVG(".$field.") AS ".$alias.", ";
		return $this;
	}

	public function distinct($field, $alias)
	{
		// TODO: Implement distinct() method.
	}

	/**
	 * Method to specify clause WHERE
	 * @param array $fields The fields concerned, with operator, and value
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('pseudo, email, age'))
	 *             ->from('informationUser', 'info')
	 *             ->where(array('idUser', '=', 1))
	 *             ->execute()
	 *             ->fetch('fetch', 'obj');
	 */
	public function where($fields = array())
	{
		$this->_req .= 'WHERE ' .$fields[0] . $fields[1] . $fields[2];

		return $this;
	}

	/**
	 * Method to specify clause ORDER
	 * @param string $field The field concerned by the order
	 * @param string $order Select the order
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('pseudo, email, age'))
	 *             ->from('informationUser', 'info')
	 *             ->where(array('idAccess', '=>', 10))
	 *             ->order('dateInscription', 'ASC')
	 *             ->execute()
	 *             ->fetch('all', 'obj');
	 */
	public function order($field, $order)
	{
		$this->_req .= "ORDER BY ".$field." ".$order;
		return $this;
	}

	/**
	 * Method to fetch the results
	 * @param string $mode The fetch method : Defined in the $this->fetch[]
	 * @param string $type The kind return fetch : Defined in the $this->typeFetch[]
	 * @acces public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return mixed
	 *
	 * Sample :
	 *  $req = $this->select(array('pseudo, email, age'))
	 *             ->from('informationUser', 'info')
	 *             ->where(array('idAccess', '=>', 10))
	 *             ->order('dateInscription', 'ASC')
	 *             ->execute()
	 *             ->fetch('all', 'obj');
	 */
	public function fetch($mode, $type)
	{
		if(!empty($this->_fetch))
		{
			$data = $this->_fetch->{$this->fetch[$mode]}($this->typeFetch[$type]);
		}

		if(!empty($this->_executed))
		{
			$data = $this->_executed->{$this->fetch[$mode]}($this->typeFetch[$type]);

		}


		return $data;
	}

	/**
	 * Method to add a new operator request (SELECT, UPDATE, INSERT, DELETE, etc...) 
	 * @param string $type What to add in the request
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors : 
	 * @package Database
	 * @return $this
	 * 
	 * Sample : 
	 * $req = $this->select(array('*'))
	 *		->from('Users', 'u')
	 *		->add('WHERE (')
	 *		->add('SELECT')
	 *		->select(array('*'))
	 *		->from('Access', 'acc')
	 *		->add(') AS test')
	 *		->add('>= 4')
	 *		->execute()
	 *      ->fetch('all', 'obj');
	 */
	public function add($type)
	{
		$this->_req .= $type . " ";

		return $this;
	}

	public function max($field, $alias)
	{

		$this->_req .= "MAX(".$field.") AS ".$alias." ";
		return $this;
	}

	public function min($field, $alias)
	{
		$this->_req .= "MIN(".$field.") AS ".$alias." ";
		return $this;
	}

	/**
	 * Method to set a new value of a update request
	 * @param string $field Name of field
	 * @param string $value The new value
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->update('Users', 'u')
	 *             ->set('username', ':new')
	 *             ->prep()
	 *             ->setParam(':new', $new_pseudo')
	 *             ->execPrepared();
	 */
	public function set($field, $value)
	{
		$this->_req .= "SET ".$field."=".$value." ";

		return $this;
	}

	/**
	 * Method to make a prepared request
	 * @param array|null $array Fill up if used with a select request
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('pseudo, password'))
					->from('Users', 'u')
					->where(array('idUser', '=', ':id'))
					->prep(array(':id' => $id))
					->fetch('single', 'obj');
	 */
	public function prep(array $array = null)
	{
		$exec = $this->getInstance()->prepare($this->_req);

		if(!empty($array))
		{
			foreach ($array as $index => $value)
			{
				$type = gettype($value);

				$exec->bindParam($index, $value, $this->typeParam[$type]);
			}

			$exec->execute();
			$this->_executed = $exec;
		}
		$this->_exec = $exec;

		return $this;
	}

	/**
	 * Method to set a value to a param in a request
	 * @param string $param Name of the param
	 * @param value $value Needs to a be a variable (reference mode)
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->insert('Users')
					->fields(array('pseudo, password, email'))
					->values(array(':pseudo, :pass, :mail'))
	 *              ->prep()
	 *              ->setParam(':pseudo', $pseudo)
	 *              ->setParam(':pass', $password)
	 *              ->setParam(':mail', $email)
					->execPrepared();
	 */
	public function setParam($param, $value)
	{
		$exec = $this->_exec;

		$type = gettype($value);

		$exec->bindParam($param, $value, $this->typeParam[$type]);

		return $this;
	}

	/**
	 * Method to execute a prepared request, only for insert, update, delete requests
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('*'))
	 *             ->from('Users')
	 *             ->where(array('idUser', '=', ':id'))
	 *             ->prep()
	 *             ->setParam(':id', 1)
	 *             ->execPrepared();
	 */
	public function execPrepared()
	{
		try
		{
			$this->_executed = $this->_exec->execute();

			return $this;
		}
		catch(\Exception $e)
		{
			$this->setErrorPDO($e);
		}
		finally
		{

		}

	}

	/**
	 * Method to execute a query
	 * @access public
	 * @version 0.0.1
	 * @author M. Tahitoa
	 * @contributors :
	 * @package Database
	 * @return $this
	 *
	 * Sample :
	 * $req = $this->select(array('*'))
	 *             ->from('Users')
	 *             ->execute()
	 *             ->fetch('all', 'obj');
	 */
	public function execute()
	{
		try
		{
			$req = $this->getInstance()->query($this->_req);

			$this->_fetch = $req;
		}catch(\Exception $e)
		{
			$this->setErrorPDO($e);
		}
		finally
		{
			return $this;
		}
	}
}
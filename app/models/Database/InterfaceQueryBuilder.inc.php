<?php
namespace App\Database;

interface QueryBuilder
{
	/**
	 * Method select
	 * @param array $fields
	 * @return mixed
	 */
	public function select($fields = array());

	/**
	 * Method update
	 * @param $table
	 * @param null $alias
	 * @return mixed
	 */
	public function update($table, $alias = null);

	/**
	 * Method delete
	 * @param $table
	 * @return mixed
	 */
	public function delete($table);

	/**
	 * Method insert
	 * @param $table
	 * @param null $alias
	 * @return mixed
	 */
	public function insert($table, $alias = null);

	/**
	 * Method from
	 * @param $table
	 * @param null $alias
	 * @return mixed
	 */
	public function from($table, $alias = null);

	/**
	 * Method join
	 * @param $table
	 * @param null $alias
	 * @return mixed
	 */
	public function join($table, $alias = null);

	/**
	 * Method where
	 * @param array $fields
	 * @return mixed
	 */
	public function where($fields = array());

	/**
	 * Method order
	 * @param $field
	 * @param $order
	 * @return mixed
	 */
	public function order($field, $order);

	/**
	 * Method sum
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function sum($field, $alias);

	/**
	 * Method count
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function count($field, $alias);

	/**
	 * Method average
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function avg($field, $alias);

	/**
	 * Method max
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function max($field, $alias);

	/**
	 * Method min
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function min($field, $alias);

	/**
	 * Method distinct
	 * @param $field
	 * @param $alias
	 * @return mixed
	 */
	public function distinct($field, $alias);

	/**
	 * Method fetch
	 * @param $mode
	 * @param $type
	 * @return mixed
	 */
	public function fetch($mode, $type);

	/**
	 * Method add
	 * @param $type
	 * @return mixed
	 */
	public function add($type);

	/**
	 * Method set
	 * @param $field
	 * @param $value
	 * @return mixed
	 */
	public function set($field, $value);

	/**
	 * Method setParam
	 * @param $param
	 * @param $value
	 * @return mixed
	 */
	public function setParam($param, $value);

	/**
	 * Method fields
	 * @param array $fields
	 * @return mixed
	 */
	public function fields($fields = array());

	/**
	 * Method values
	 * @param array $value
	 * @return mixed
	 */
	public function values($value = array());
}
<?php
namespace App\Models;
/**
 * Class Router
 */

//Loading the Database File :
use App\Controllers\ErrorClass;
use App\Database\Database;

class Router extends Database
{
	protected $_controller;
	protected $_action;
	protected $_sub_action = array();
	protected $url;
	protected $_admin;

	public function __construct(string $url)
	{
		parent::__construct();
		$this->setUrl($url);
		$this->explodeUrl();
		$this->loadController($this->getController(), $this->getAction());
	}

	protected function setUrl(string $url)
	{
		$this->url = $url;
	}

	protected function defineAdmin()
	{
		if(explode('\\', get_called_class())[2] == "Admin")
		{
			$this->_admin = TRUE;
		}
		else
		{
			$this->_admin = FALSE;
		}
	}

	public function getAdminStatus()
	{
		return $this->_admin;
	}

	private function explodeUrl()
	{
		$array = explode('/', $this->url);

		if(isset($array[0]) && !empty($array[0]))
		{
			$this->setController($array[0]);
		} else
		{
			$this->setController('index');
		}

		if(isset($array[1]) && !empty($array[1]))
		{
			$this->setAction($array[1]);
		} else
		{
			$this->setAction('index');
		}
	}

	private function setController(string $controller)
	{
		$this->_controller = $this->escapeString($controller);
	}

	private function setAction(string $action)
	{
		$this->_action = $this->escapeString($action);
	}

	protected function getController() : string
	{
		return $this->_controller;
	}

	protected function getAction() : string
	{
		return $this->_action;
	}

	protected function loadController(string $controller, string $action)
	{
		$controller = $this->escapeString(ucfirst($controller));
		$action = $this->escapeString($action);
		$dir = ROOT.'/app/controllers'.'/'.$controller.'/'.$controller.'.class.php';
		
		try
		{
			if(file_exists($dir))
			{
				$controller ='App\Controllers\\'.$controller;

				if(class_exists($controller) && method_exists($controller, $action))
				{

					$controller = new $controller();
					$controller->$action($this->_sub_action);
				}
				else
				{
					$err = new ErrorClass();
					$err->index();
					throw new \Exception('No class or method');
				}
			} else
			{
				$err = new ErrorClass();
				$err->index();

				throw new \Exception("Missing file");
			}
		}
		catch(\Exception $e)
		{
			$this->setErrorPDO($e);
		}
	}

	public function render(array $arrayMerge = null)
	{
		$calledClass = explode('\\', get_called_class());
		$this->defineAdmin();

		try
		{
			$regex = '/^([a-zA-Z]).+/i';
			$files = glob('views/'.$calledClass[2].'/*.tpl.php');

			$result = array();
			$i = 0;

			foreach($files as $index => $file)
			{
				preg_match($regex, $file, $match);
				$str = explode('/', $file);

				$result[$i] = substr($str[2], 0, -8);
				$i++;
			}

			$file = '';

			foreach($result as $key => $value)
			{
				$test = preg_match('/^'.$value.'/i', $calledClass[2], $match);

				if($test)
				{
					$file = $value;
				}
			}

			if(file_exists('views/'.$calledClass[2].'/'.$file.'.tpl.php'))
			{
				$view = 'views/'.$calledClass[2].'/'.$file.'.tpl.php';
			} else
			{
				throw new \Exception('No file view');
			}

			$default = array(
				'content' => $view,
			);

			if(isset($arrayMerge))
			{
				$array = array_merge($default, $arrayMerge);
			} else
			{
				$array = $default;
			}

			extract($array);

			require_once 'views/template.tpl.php';
		}
		catch(\Exception $e)
		{
			$err = new ErrorClass();
			$err->index();

			$this->setErrorPDO($e);
		}
	}

	public function escapeString(string $string) : string
	{
		$string = htmlspecialchars($string, ENT_HTML5, 'UTF-8');

		return $string;
	}

	public function setSession(string $key, $value)
	{
		$_SESSION[$key] = $this->escapeString($value);
	}

	public function deleteSession(string $key)
	{
		unset($_SESSION[$key]);
	}
}
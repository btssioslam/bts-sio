<!DOCTYPE html>
<html lang="fr">
<head>
	<meta name="author" content="MERLIN Tahitoa">
	<meta charset="utf-8">
	<meta name="device" content="width=device-width,initial-scale=1">
	<meta name="content" content="text/html">
	<meta name="description" content="Projet communautaire du groupe de BTS SIO du CNED">
	<link rel="stylesheet" href="/webroot/libraries/bootstrap-3.3.6-dist/css/bootstrap.min.css" />
	<script type="text/javascript" src="/webroot/scripts/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="/webroot/libraries/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/webroot/scripts/angular.min.js"></script>
	<script type="text/javascript" src="/webroot/scripts/custom/clients_scripts.js"></script>
	<link rel="stylesheet" href="/webroot/styles/style.css" />
	<?php
	if(isset($scripts))
	{
		foreach($scripts as $script) : ?>
			<script type="text/javascript" src="/webroot/scripts/<?php print $script; ?>"></script>
		<?php endforeach;
	}
	?>
	<title><?php print isset($title) ? $title : 'Projet BTS SIO - Groupe CNED'; ?></title>
</head>

<body>
<?php include 'templates/header.tpl.php'; ?>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
	<?php include 'templates/menu.tpl.php'; ?>
</div>

<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
	<?php include $content; ?>
	<?php if(isset($tpl))
	{
		if(file_exists('views/'.get_called_class().'/'.$tpl.'.php'))
		{
			include 'views/'.get_called_class().'/'.$tpl.'.php';
		}
	}
	?>
</div>
<div class="clearfix"></div>
<?php include 'templates/footer.tpl.php'; ?>
</body>
</html>
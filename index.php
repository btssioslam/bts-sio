<?php
//We define the environment mode to display or not the errors
//We assume TRUE to define the development environment
define('ENV', TRUE);

if(ENV)
{
	ini_set('startup_display_errors', E_ALL);
	ini_set('display_errors', E_ALL);
	ini_set('error_reporting', E_ALL);
	ini_set('html_errors', E_ALL);
}

//Defining the root dir
define('ROOT', $_SERVER['DOCUMENT_ROOT']);

//We start the session for the whole site
session_start();

require_once 'vendor/autoload.php';

//We include the required files :
//We initialize the router :
//@TODO : Develop the router
$router = new App\Models\Router($_GET['c']);
CREATE DATABASE IF NOT EXISTS btsdev CHARACTER SET = UTF8 COLLATE utf8_bin;

USE btsdev;

CREATE TABLE IF NOT EXISTS Error
(
	idError integer(20) AUTO_INCREMENT,
	messageError varchar(255) NOT NULL COLLATE utf8_bin,
	codeError varchar(40) NOT NULL COLLATE utf8_bin,
	file varchar(255) NOT NULL COLLATE utf8_bin,
	lineFile integer(10) NOT NULL COLLATE utf8_bin,
	PRIMARY KEY pk_error(idError)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Users
(
	idUser varchar(20) NOT NULL COLLATE utf8_bin,
	pseudo varchar(20) NOT NULL COLLATE utf8_bin,
	password varchar(255) NOT NULL COLLATE utf8_bin,
	email varchar(80) NOT NULL COLLATE utf8_bin,
	dateInscription DATETIME,
	idAccess integer(3) DEFAULT 0,
	PRIMARY KEY pk_user(idUser)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS InformationUser
(
	idUser varchar(20) NOT NULL COLLATE utf8_bin,
	nom varchar(60) DEFAULT NULL COLLATE utf8_bin,
	prenom varchar(60) DEFAULT NULL COLLATE utf8_bin,
	formation varchar(120) DEFAULT NULL COLLATE utf8_bin,
	anneeFormation int(2) DEFAULT NULL,
	sexe char(1) DEFAULT NULL COLLATE utf8_bin,
	age int(3) DEFAULT NULL COLLATE utf8_bin,
	dateNaissance DATE DEFAULT NULL,
	PRIMARY KEY pk_user(idUser)
)CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Access
(
	idAccess INTEGER(3) NOT NULL,
	nomAcess varchar(20) NOT NULL COLLATE utf8_bin,
	levelAccess integer(5) DEFAULT 0,
	PRIMARY KEY pk_access(idAccess)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Messages
(
	idMessage varchar(20) NOT NULL COLLATE utf8_bin,
	titleMessage varchar(150) NOT NULL COLLATE utf8_bin,
	authorMessage varchar(20) NOT NULL COLLATE utf8_bin,
	receiverMessage varchar(20) NOT NULL COLLATE utf8_bin,
	textMessage text NOT NULL COLLATE utf8_bin,
	statusMessage BOOLEAN DEFAULT FALSE,
	dateMessage DATETIME,
	PRIMARY KEY pk_message(idMessage)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Articles
(
	idArticle integer(20) NOT NULL,
	titleArticle varchar(50) NOT NULL COLLATE utf8_bin,
	descArticle varchar(120) DEFAULT NULL COLLATE utf8_bin,
	textArticle LONGTEXT NOT NULL,
	pubArticle TIMESTAMP,
	updateArticle TIMESTAMP,
	consultation integer(10) DEFAULT 0,
	author varchar(20) NOT NULL COLLATE utf8_bin,
	category varchar(20) NOT NULL COLLATE utf8_bin,
	PRIMARY KEY pk_article(idArticle)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Comments
(
	idComment varchar(20) NOT NULL COLLATE utf8_bin,
	titleComment varchar(50) DEFAULT NULL COLLATE utf8_bin,
	textComment text NOT NULL COLLATE utf8_bin,
	dateComment TIMESTAMP,
	authorComment varchar(20) NOT NULL COLLATE utf8_bin,
	articleComment integer(20) NOT NULL COLLATE utf8_bin,
	PRIMARY KEY pk_comment(idComment)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Categories
(
	idCategory varchar(20) NOT NULL COLLATE utf8_bin,
	titleCategpory varchar(40) NOT NULL COLLATE utf8_bin,
	descCategory varchar(100) DEFAULT NULL COLLATE utf8_bin,
	weightCategory integer(4) DEFAULT 0,
	PRIMARY KEY pk_category(idCategory)
) CHARACTER SET = UTF8 COLLATE utf8_bin, ENGINE=InnoDB;

ALTER TABLE Users
ADD CONSTRAINT FOREIGN KEY fk_access(idAccess) REFERENCES Access(idAccess) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE InformationUser
ADD CONSTRAINT FOREIGN KEY fk_user(idUser) REFERENCES Users(idUser) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Messages
ADD CONSTRAINT FOREIGN KEY fk_author(authorMessage) REFERENCES Users(idUser) ON UPDATE CASCADE ON DELETE CASCADE,
ADD CONSTRAINT FOREIGN KEY fk_receiver(receiverMessage) REFERENCES Users(idUser) ON UPDATE CASCADE ON DELETE CASCADE ;

ALTER TABLE Articles
ADD CONSTRAINT FOREIGN KEY fk_art_author(author) REFERENCES Users(idUser) ON UPDATE CASCADE ON DELETE CASCADE,
ADD CONSTRAINT FOREIGN KEY fk_category(category) REFERENCES Categories(idCategory) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE Comments
ADD CONSTRAINT FOREIGN KEY fk_author_comment(authorComment) REFERENCES Users(idUser) ON UPDATE CASCADE ON DELETE CASCADE ,
ADD CONSTRAINT FOREIGN KEY fk_article_comment(articleComment) REFERENCES Articles(idArticle) ON UPDATE CASCADE ON DELETE CASCADE;

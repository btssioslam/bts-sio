<?php
/**
 * Contains the settings of the database
 * Edit what you need to work with your database
 * The index env, by default we assume TRUE is the dev mode, FALSE for prod
 */

$database = array(
	'driver' => 'mysql',
	'host' => 'localhost',
	'database' => 'btsdev',
	'login' => 'bts-dev',
	'password' => 'BTS$10D3V',
	'env' => TRUE,
);

//var_dump($database);
extract($database);